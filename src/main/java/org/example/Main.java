package org.example;

import org.example.banknote.CurrencyType;

/**
 * Main.
 *
 * @author Ilya_Sukhachev
 */
public class Main {

    public static void main(String[] args) {
        Bancomat bancomat = new Bancomat();

        System.out.println("К выдаче: " + bancomat.withdraw(CurrencyType.USD, "12"));
        System.out.println("К выдаче: " + bancomat.withdraw(CurrencyType.USD, "60"));
        System.out.println("К выдаче: " + bancomat.withdraw(CurrencyType.USD, "930"));

        System.out.println("-------------------------");

        System.out.println("К выдаче: " + bancomat.withdraw(CurrencyType.RUB, "13"));
        System.out.println("К выдаче: " + bancomat.withdraw(CurrencyType.RUB, "7700"));
        System.out.println("К выдаче: " + bancomat.withdraw(CurrencyType.RUB, "12200"));

        System.out.println("-------------------------");

        System.out.println("К выдаче: " + bancomat.withdraw(CurrencyType.EUR, "100"));
        System.out.println("К выдаче: " + bancomat.withdraw(CurrencyType.USD, null));
        System.out.println("К выдаче: " + bancomat.withdraw(CurrencyType.USD, "sdfsdf"));
        System.out.println("К выдаче: " + bancomat.withdraw(CurrencyType.USD, "0"));
    }
}
