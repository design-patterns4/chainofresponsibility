package org.example.dollar;

import org.example.banknote.Banknote;
import org.example.banknote.BanknoteHandler;

import java.util.List;

/**
 * DollarHandlerBase.
 *
 * @author Ilya_Sukhachev
 */
public abstract class DollarHandlerBase extends BanknoteHandler {
    protected DollarHandlerBase(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    public boolean validate(String banknote) {
        if (banknote.equals(String.valueOf(getValue()))) {
            return true;
        }
        return super.validate(banknote);
    }

    @Override
    public List<Banknote> withdraw(Integer banknote) {
        int value = getValue();
        int possibleQuantity = banknote / value;
        if(possibleQuantity >= 1) {
            add(getBanknote(), possibleQuantity);
            return super.withdraw(banknote - possibleQuantity * value);
        }
        return super.withdraw(banknote);
    }

    protected abstract int getValue();

    protected abstract Banknote getBanknote();
}
