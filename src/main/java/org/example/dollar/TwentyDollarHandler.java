package org.example.dollar;

import org.example.banknote.Banknote;
import org.example.banknote.BanknoteHandler;
import org.example.banknote.dollar.TwentyDollar;

/**
 * TwentyDollarHandler.
 *
 * @author Tatyana_Dolnikova
 */
public class TwentyDollarHandler extends DollarHandlerBase {

    protected int value = 20;

    public TwentyDollarHandler(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected int getValue() {
        return value;
    }

    @Override
    protected Banknote getBanknote() {
        return new TwentyDollar();
    }
}
