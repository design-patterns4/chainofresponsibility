package org.example.banknote;

import lombok.var;

import java.util.ArrayList;
import java.util.List;

/**
 * BanknoteHandler.
 *
 * @author Ilya_Sukhachev
 */
public abstract class BanknoteHandler {

    private BanknoteHandler nextHandler;
    private static List<Banknote> banknotes = new ArrayList<>();

    protected BanknoteHandler(BanknoteHandler nextHandler) {
        this.nextHandler = nextHandler;
    }

    public boolean validate(String banknote) {
        return nextHandler != null && nextHandler.validate(banknote);
    }

    public List<Banknote> withdraw(Integer banknote) {
        if (banknote == 0) {
            var result = new ArrayList<>(banknotes);
            banknotes.clear();
            return result;
        }
        if (nextHandler == null) {
            banknotes.clear();
            System.out.print("Введите другую сумму. ");
            return banknotes;
        }
        return nextHandler.withdraw(banknote);
    }

    public void add(Banknote banknote, int quantity) {
        if (banknotes == null) {
            banknotes = new ArrayList<>();
        }
        for (int i = 0; i < quantity; i++) {
            banknotes.add(banknote);
        }
    }

}
