package org.example.banknote;

/**
 * CurrencyType.
 *
 * @author Ilya_Sukhachev
 */
public enum CurrencyType {
    EUR("евро", "евро"),
    USD("доллар", "долларов"),
    RUB("рубль", "рублей");

    CurrencyType(String name, String genetive) {
        this.name = name;
        this.genetive = genetive;
    }

    private final String name;
    private final String genetive;

    public String getName() {
        return name;
    }

    public String getGenetive() {
        return genetive;
    }
}
