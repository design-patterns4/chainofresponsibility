package org.example.banknote.ruble;

/**
 * HundredRuble.
 *
 * @author Tatyana_Dolnikova
 */
public class HundredRuble extends RubleBanknote {

    public HundredRuble() {
        super(100);
    }

    @Override
    public int getValue() {
        return value;
    }

}
