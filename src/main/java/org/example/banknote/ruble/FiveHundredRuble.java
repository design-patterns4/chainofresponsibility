package org.example.banknote.ruble;

/**
 * FiftyDollar.
 *
 * @author Tatyana_Dolnikova
 */
public class FiveHundredRuble extends RubleBanknote {

    public FiveHundredRuble() {
        super(500);
    }

    @Override
    public int getValue() {
        return value;
    }
}
