package org.example.banknote.ruble;

/**
 * FiveThousandRuble.
 *
 * @author Tatyana_Dolnikova
 */
public class FiveThousandRuble extends RubleBanknote {

    public FiveThousandRuble() {
        super(5000);
    }

    @Override
    public int getValue() {
        return value;
    }

}