package org.example.banknote.ruble;

import org.example.banknote.Banknote;
import org.example.banknote.CurrencyType;

/**
 * RubleBanknote.
 *
 * @author Tatyana_Dolnikova
 */
public abstract class RubleBanknote extends Banknote {

    protected final static CurrencyType currency = CurrencyType.RUB;

    public RubleBanknote(int value) {
        super(currency, value);
    }

    @Override
    public CurrencyType getCurrency() {
        return currency;
    }
}
