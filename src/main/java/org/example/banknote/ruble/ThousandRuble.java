package org.example.banknote.ruble;

/**
 * ThousandRuble.
 *
 * @author Tatyana_Dolnikova
 */
public class ThousandRuble extends RubleBanknote {

    public ThousandRuble() {
        super(1000);
    }

    @Override
    public int getValue() {
        return value;
    }
}
