package org.example.banknote.ruble;

/**
 * TwoThousandRuble.
 *
 * @author Tatyana_Dolnikova
 */
public class TwoThousandRuble extends RubleBanknote {

    public TwoThousandRuble() {
        super(2000);
    }

    @Override
    public int getValue() {
        return value;
    }
}
