package org.example.banknote;

/**
 * Banknote.
 *
 * @author Ilya_Sukhachev
 */
public abstract class Banknote {

    protected CurrencyType currency;
    protected int value;

    public Banknote(CurrencyType currency, int value) {
        this.currency = currency;
        this.value = value;
    }

    public CurrencyType getCurrency() {
        return currency;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return getValue() + " " + getCurrency().getGenetive();
    }
}
