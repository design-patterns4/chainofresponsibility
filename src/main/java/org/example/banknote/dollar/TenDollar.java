package org.example.banknote.dollar;

/**
 * TenDollar.
 *
 * @author Tatyana_Dolnikova
 */
public class TenDollar extends DollarBanknote {

    public TenDollar() {
        super(10);
    }

    @Override
    public int getValue() {
        return value;
    }
}
