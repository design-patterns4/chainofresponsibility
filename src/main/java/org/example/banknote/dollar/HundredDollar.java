package org.example.banknote.dollar;

/**
 * HundredDollar.
 *
 * @author Tatyana_Dolnikova
 */
public class HundredDollar extends DollarBanknote {

    public HundredDollar() {
        super(100);
    }

    @Override
    public int getValue() {
        return value;
    }

}
