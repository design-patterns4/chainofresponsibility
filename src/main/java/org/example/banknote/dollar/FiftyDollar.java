package org.example.banknote.dollar;

/**
 * FiftyDollar.
 *
 * @author Tatyana_Dolnikova
 */
public class FiftyDollar extends DollarBanknote {

    public FiftyDollar() {
        super(50);
    }

    @Override
    public int getValue() {
        return value;
    }
}
