package org.example.banknote.dollar;

/**
 * TwentyDollar.
 *
 * @author Tatyana_Dolnikova
 */
public class TwentyDollar extends DollarBanknote {

    public TwentyDollar() {
        super(20);
    }

    @Override
    public int getValue() {
        return value;
    }
}
