package org.example.banknote.dollar;

import org.example.banknote.Banknote;
import org.example.banknote.CurrencyType;

/**
 * DollarBanknote.
 *
 * @author Tatyana_Dolnikova
 */
public abstract class DollarBanknote extends Banknote {

    protected final static CurrencyType currency = CurrencyType.USD;

    public DollarBanknote(int value) {
        super(currency, value);
    }

    @Override
    public CurrencyType getCurrency() {
        return currency;
    }
}
