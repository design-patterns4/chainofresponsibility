package org.example;

import org.apache.commons.lang3.ObjectUtils;
import org.example.banknote.Banknote;
import org.example.banknote.BanknoteHandler;
import org.example.banknote.CurrencyType;
import org.example.dollar.FiftyDollarHandler;
import org.example.dollar.HundredDollarHandler;
import org.example.dollar.TenDollarHandler;
import org.example.dollar.TwentyDollarHandler;
import org.example.ruble.FiveHundredRubleHandler;
import org.example.ruble.FiveThousandRubleHandler;
import org.example.ruble.HundredRubleHandler;
import org.example.ruble.ThousandRubleHandler;
import org.example.ruble.TwoThousandRubleHandler;

import java.util.List;

/**
 * Bancomat.
 *
 * @author Ilya_Sukhachev
 */
public class Bancomat {
    private BanknoteHandler dollarHandler;
    private BanknoteHandler rubleHandler;

    public Bancomat() {
        dollarHandler = new TenDollarHandler(null);
        dollarHandler = new TwentyDollarHandler(dollarHandler);
        dollarHandler = new FiftyDollarHandler(dollarHandler);
        dollarHandler = new HundredDollarHandler(dollarHandler);

        rubleHandler = new HundredRubleHandler(null);
        rubleHandler = new FiveHundredRubleHandler(rubleHandler);
        rubleHandler = new ThousandRubleHandler(rubleHandler);
        rubleHandler = new TwoThousandRubleHandler(rubleHandler);
        rubleHandler = new FiveThousandRubleHandler(rubleHandler);
    }

    public boolean validate(CurrencyType currency, String banknote) {
        if (ObjectUtils.anyNull(currency, banknote) || banknote.isEmpty()) {
            return false;
        }
        switch (currency) {
            case USD:
                return dollarHandler.validate(banknote);
            case RUB:
                return rubleHandler.validate(banknote);
            default:
                currencyNotSupportedWarning(currency);
                return false;
        }
    }

    public List<Banknote> withdraw(CurrencyType currency, String banknote) {
        if (ObjectUtils.anyNull(currency, banknote) || banknote.isEmpty()) {
            return null;
        }

        int sum = 0;
        try {
            sum = Integer.parseInt(banknote);
        } catch (NumberFormatException e) {
            System.out.println("Недопустимая сумма: " + banknote);
            return null;
        }

        switch (currency) {
            case USD:
                return dollarHandler.withdraw(sum);
            case RUB:
                return rubleHandler.withdraw(sum);
            default:
                currencyNotSupportedWarning(currency);
                return null;
        }
    }

    private static void currencyNotSupportedWarning(CurrencyType currency) {
        System.out.print("Валюта " + currency.getName() + " не поддерживается. ");
    }

}
