package org.example.ruble;

import org.example.banknote.Banknote;
import org.example.banknote.BanknoteHandler;
import org.example.banknote.ruble.FiveThousandRuble;
import org.example.banknote.ruble.HundredRuble;

/**
 * HundredRubleHandler.
 *
 * @author Tatyana_Dolnikova
 */
public class HundredRubleHandler extends RubleHandlerBase {

    protected int value = 100;

    public HundredRubleHandler(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected int getValue() {
        return value;
    }

    @Override
    protected Banknote getBanknote() {
        return new HundredRuble();
    }
}
