package org.example.ruble;

import org.example.banknote.Banknote;
import org.example.banknote.BanknoteHandler;
import org.example.banknote.ruble.TwoThousandRuble;

/**
 * ThousandRubleHandler.
 *
 * @author Tatyana_Dolnikova
 */
public class TwoThousandRubleHandler extends RubleHandlerBase {

    protected int value = 2000;

    public TwoThousandRubleHandler(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected int getValue() {
        return value;
    }

    @Override
    protected Banknote getBanknote() {
        return new TwoThousandRuble();
    }
}
