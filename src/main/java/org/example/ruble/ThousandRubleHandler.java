package org.example.ruble;

import org.example.banknote.Banknote;
import org.example.banknote.BanknoteHandler;
import org.example.banknote.ruble.ThousandRuble;

/**
 * ThousandRubleHandler.
 *
 * @author Tatyana_Dolnikova
 */
public class ThousandRubleHandler extends RubleHandlerBase {

    protected int value = 1000;

    public ThousandRubleHandler(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected int getValue() {
        return value;
    }

    @Override
    protected Banknote getBanknote() {
        return new ThousandRuble();
    }
}
