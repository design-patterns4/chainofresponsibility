package org.example.ruble;

import org.example.banknote.Banknote;
import org.example.banknote.BanknoteHandler;
import org.example.banknote.ruble.FiveHundredRuble;
import org.example.banknote.ruble.FiveThousandRuble;

/**
 * FiveThousandRubleHandler.
 *
 * @author Tatyana_Dolnikova
 */
public class FiveThousandRubleHandler extends RubleHandlerBase {

    protected int value = 5000;

    public FiveThousandRubleHandler(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected int getValue() {
        return value;
    }

    @Override
    protected Banknote getBanknote() {
        return new FiveThousandRuble();
    }
}
