package org.example.ruble;

import org.example.banknote.Banknote;
import org.example.banknote.BanknoteHandler;
import org.example.banknote.ruble.FiveHundredRuble;

/**
 * HundredDollarHandler.
 *
 * @author Tatyana_Dolnikova
 */
public class FiveHundredRubleHandler extends RubleHandlerBase {

    protected int value = 500;

    public FiveHundredRubleHandler(BanknoteHandler nextHandler) {
        super(nextHandler);
    }

    @Override
    protected int getValue() {
        return value;
    }

    @Override
    protected Banknote getBanknote() {
        return new FiveHundredRuble();
    }
}
